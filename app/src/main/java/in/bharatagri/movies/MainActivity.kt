package `in`.bharatagri.movies

import `in`.bharatagri.movies.adapter.MainAdapter
import `in`.bharatagri.movies.viewModel.MainViewModel
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_progress_recyclerview.view.*


class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        initObservers()
    }

    private fun initObservers() {
        val mainAdapter = MainAdapter(this)
        mainAdapter.onItemClick = {
            DetailActivity.startActivity(this, it.movieId)
        }
        progressRecyclerView.recyclerView.adapter = mainAdapter
        progressRecyclerView.recyclerView.layoutManager = GridLayoutManager(this, 2)

        progressRecyclerView.swipeRefreshLayout.setOnRefreshListener { requestMovies() }

        requestMovies()
        mainViewModel.isLoading.observe(this) {
            if (it != null) {
                if (it) progressRecyclerView.showProgress() else progressRecyclerView.hideProgress()
            }
        }
        mainViewModel.movieList.observe(this) {
            if (it != null && it.isNotEmpty()) {
                mainAdapter.movieList = ArrayList(it)
                progressRecyclerView.showRecyclerView()
            } else {
                progressRecyclerView.showError()
            }
        }

        radioGroupSort.setOnCheckedChangeListener { _, _ -> requestMovies() }

    }

    private fun requestMovies() {
        val checkedId = radioGroupSort.checkedRadioButtonId
        if (checkedId == R.id.radioButtonLatest) {
            mainViewModel.requestLatestMovies()
        }
        if (checkedId == R.id.radioButtonPopular) {
            mainViewModel.requestPopularMovies()
        }
        if (checkedId == R.id.radioButtonRating) {
            mainViewModel.requestVotedMovies()
        }
    }
}