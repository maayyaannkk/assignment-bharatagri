package `in`.bharatagri.movies.adapter

import `in`.bharatagri.movies.R
import `in`.bharatagri.movies.data.Model
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.list_item_movie.view.*

class MainAdapter(private val context: Context) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    var onItemClick: ((Model.MovieModel) -> Unit)? = null
    var movieList: ArrayList<Model.MovieModel> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_movie, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val posterPost = movieList[position].posterImage
        Glide.with(context).load("http://image.tmdb.org/t/p/w342$posterPost")
            .error(R.drawable.ic_error)
            .into(holder.imageViewPoster)
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(movieList[position])
        }
    }

    inner class ViewHolder constructor(v: View) : RecyclerView.ViewHolder(v) {
        val imageViewPoster: ImageView = v.imageViewPoster
    }
}