package `in`.bharatagri.movies.custom

import `in`.bharatagri.movies.R
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.custom_progress_recyclerview.view.*

class ProgressRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    init {
        View.inflate(getContext(), R.layout.custom_progress_recyclerview, this)
        recyclerView.layoutManager = LinearLayoutManager(getContext())
    }

    fun showProgress() {
        swipeRefreshLayout.isRefreshing = true
    }

    fun hideProgress() {
        swipeRefreshLayout.isRefreshing = false
    }

    fun showError() {
        hideProgress()
        recyclerView.visibility = View.GONE
        imageViewError.visibility = View.VISIBLE
        textViewError.visibility = View.VISIBLE
    }

    fun showRecyclerView() {
        hideProgress()
        recyclerView.visibility = View.VISIBLE
        imageViewError.visibility = View.GONE
        textViewError.visibility = View.GONE
    }
}