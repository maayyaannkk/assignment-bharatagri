package `in`.bharatagri.movies.network

import `in`.bharatagri.movies.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val RetrofitService: NetworkService = Retrofit.Builder()
    .baseUrl("https://api.themoviedb.org/3/")
    .client(getClient())
    .addConverterFactory(
        GsonConverterFactory.create(
            GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        )
    )
    .build().create(NetworkService::class.java)

fun getClient(): OkHttpClient {
    val httpClient = OkHttpClient.Builder()
    httpClient.addInterceptor {
        val originalHttpUrl: HttpUrl = it.request().url
        val url = originalHttpUrl.newBuilder()
            .addQueryParameter("api_key", BuildConfig.API_KEY)
            .build()
        val requestBuilder: Request.Builder = it.request().newBuilder()
            .url(url)
        val request: Request = requestBuilder.build()
        it.proceed(request)
    }
    return httpClient.build()
}

