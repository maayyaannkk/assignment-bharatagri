package `in`.bharatagri.movies.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface NetworkService {
    @GET("discover/movie?sort_by=popularity.desc")
    fun requestPopularMovies(): Call<ResponseBody>

    @GET("discover/movie?sort_by=vote_average.desc")
    fun requestVotedMovies(): Call<ResponseBody>

    @GET("discover/movie?sort_by=release_date.desc")
    fun requestLatestMovies(): Call<ResponseBody>
}