package `in`.bharatagri.movies.viewModel

import `in`.bharatagri.movies.data.Model
import `in`.bharatagri.movies.data.MovieDatabase
import `in`.bharatagri.movies.network.RetrofitService
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val database = MovieDatabase.getInstance(getApplication())
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    var movieList: MutableLiveData<List<Model.MovieModel>> = MutableLiveData()

    //sort order 1
    fun requestPopularMovies() {
        getMovies(1)
        isLoading.postValue(true)
        RetrofitService.requestPopularMovies().enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                isLoading.postValue(false)
                if (response.isSuccessful) {
                    insertResponse(response)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                isLoading.postValue(false)
            }
        })
    }

    //sort order 2
    fun requestVotedMovies() {
        getMovies(2)
        isLoading.postValue(true)
        RetrofitService.requestVotedMovies().enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                isLoading.postValue(false)
                if (response.isSuccessful) {
                    insertResponse(response)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                isLoading.postValue(false)
            }
        })
    }

    //sort order 3
    fun requestLatestMovies() {
        getMovies(3)
        isLoading.postValue(true)
        RetrofitService.requestLatestMovies().enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                isLoading.postValue(false)
                if (response.isSuccessful) {
                    insertResponse(response)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                isLoading.postValue(false)
            }
        })
    }

    private fun insert(movieList: List<Model.MovieModel>) {
        viewModelScope.launch(Dispatchers.IO) {
            database.movieDao().insertAll(movieList)
        }
    }

    private fun getMovies(sortOrder: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if (sortOrder == 1)
                movieList.postValue(database.movieDao().getMoviePopular())
            if (sortOrder == 2)
                movieList.postValue(database.movieDao().getMovieRating())
            if (sortOrder == 3)
                movieList.postValue(database.movieDao().getMovieNewest())
        }
    }

    private fun insertResponse(response: Response<ResponseBody>) {
        val jsonObject = JSONObject(response.body()!!.string())
        val data = jsonObject.getJSONArray("results")
        try {
            val movieList: java.util.ArrayList<Model.MovieModel> =
                GsonBuilder().setDateFormat("yyyy-MM-dd").serializeNulls().create().fromJson(
                    data.toString(),
                    object : TypeToken<List<Model.MovieModel>>() {}.type
                )
            insert(movieList)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}