package `in`.bharatagri.movies.viewModel

import `in`.bharatagri.movies.data.Model
import `in`.bharatagri.movies.data.MovieDatabase
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(application: Application) : AndroidViewModel(application) {
    private val database = MovieDatabase.getInstance(getApplication())

    fun getMovie(movieId: Int): LiveData<Model.MovieModel> {
        return database.movieDao().getMovie(movieId)
    }
}