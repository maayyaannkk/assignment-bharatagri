package `in`.bharatagri.movies.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MovieDao {
    @Query("SELECT * from movie_table ORDER BY popularity Desc")
    suspend fun getMoviePopular(): List<Model.MovieModel>

    @Query("SELECT * from movie_table ORDER BY releaseDate Desc")
    suspend fun getMovieNewest(): List<Model.MovieModel>

    @Query("SELECT * from movie_table ORDER BY voteAverage Desc")
    suspend fun getMovieRating(): List<Model.MovieModel>

    @Query("SELECT * from movie_table where movieId=:movieId")
    fun getMovie(movieId: Int): LiveData<Model.MovieModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(movieModelList: List<Model.MovieModel>)
}