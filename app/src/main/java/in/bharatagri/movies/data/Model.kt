package `in`.bharatagri.movies.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

//Wrapping MovieModel class in Model object as that allows preserve the class easily from proguard obfuscate
object Model {
    @Entity(tableName = "movie_table")
    data class MovieModel(
        @PrimaryKey @SerializedName("id") val movieId: Int,
        @SerializedName("poster_path") val posterImage: String?,
        @SerializedName("backdrop_path") val backImage: String?,
        @SerializedName("original_title") val movieTitle: String?,
        @SerializedName("overview") val overView: String?,
        @SerializedName("vote_average") val voteAverage: Double?,
        @SerializedName("popularity") val popularity: Double?,
        @SerializedName("release_date") val releaseDate: Date?
    )
}