package `in`.bharatagri.movies

import `in`.bharatagri.movies.viewModel.DetailViewModel
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import java.text.SimpleDateFormat
import java.util.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        if (intent.hasExtra(EXTRA_MOVIE_ID)) {
            initObserver(intent.getIntExtra(EXTRA_MOVIE_ID, 0))
        } else finish()

    }

    private fun initObserver(movieId: Int) {
        val detailViewModel = ViewModelProvider(this).get(DetailViewModel::class.java)
        detailViewModel.getMovie(movieId).observe(this) {
            if (it != null) {
                val backDropImage = it.backImage
                Glide.with(this).load("http://image.tmdb.org/t/p/w342$backDropImage")
                    .error(R.drawable.ic_error)
                    .into(imageViewBackDrop)
                textViewMovieName.text = it.movieTitle
                textViewSynopsis.text = it.overView
                if (it.releaseDate != null) {
                    textViewYear.text =
                        SimpleDateFormat("dd MMMM, yyyy", Locale.ENGLISH).format(it.releaseDate)
                }
                textViewRating.text = it.voteAverage.toString()
                textViewPopularity.text = String.format(Locale.ENGLISH, "%.2f votes", it.popularity)
                title = it.movieTitle
            }
        }
    }

    companion object {
        private const val EXTRA_MOVIE_ID = "extra_movie_id"

        fun startActivity(activity: Activity, movieId: Int) {
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra(EXTRA_MOVIE_ID, movieId)
            activity.startActivity(intent)
        }
    }

}